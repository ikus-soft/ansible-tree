# Installation

```{warning}
**Important Considerations Before Installing**

Before installing SDL on an existing system, it is essential to evaluate whether SDL's default configuration might disrupt your server or workstation. For example, SDL's default configuration for `openssh-server` enforces stricter security settings by disabling the use of RSA keys. Make sure these changes align with your system requirements.
```

## Installation Steps

To install SDL, follow these steps:

```bash
sudo apt update
sudo apt install sdl sdl-roles
```

## Post-Installation Behavior

After installation, any use of the `apt` commands (`apt upgrade`, `apt remove`, or `apt install`) will automatically trigger the execution of `sdl-apply`. This ensures that SDL's configuration is applied consistently across your system.
