# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Software Defined Linux'
copyright = '2025, Patrik Dufresne'
author = 'Patrik Dufresne'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'myst_parser',
]

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']


#
# Generate index of roles base on README files and defaults/main.yml
#
import shutil
from pathlib import Path

cwd = Path(".")
roles_path = Path('../roles')

# Delete previously generated docs.
shutil.rmtree(cwd / "roles", ignore_errors=True)

# Find all README.md files in source code
roles_index={}
for path in roles_path.glob("*/packages/*/*"):
    readme_md = path / 'README.md'
    defaults_yml = (path / 'defaults').glob('*.yml')
    # New MD file
    new_md = Path(str(readme_md).lstrip('../')[:-10] + ".md")
    unused, distribution, unused, source_package, binary_package = str(new_md).split('/')

    # Copy or create README file into doc folder
    new_md.parent.mkdir(parents=True, exist_ok=True)
    if readme_md.exists():
        shutil.copy(readme_md, new_md)
    else:
        with open(new_md, 'a') as f:
            f.write(f"# {binary_package[:-3]}\n\n")

    # Copy defaults into README file.
    if defaults_yml:
        with open(new_md, 'a') as f:
            f.write(f"## Available Defaults\n")
            for default in defaults_yml:
                default_data = default.read_text()
                # Append defaults variable to README file.
                f.write(f"## `{default.name[:-4]}` default\n")
                f.write(f"```\n{default_data}\n```\n")

    # If new README file got created, add it to the index.
    if new_md.exists():
        roles_index.setdefault(distribution,{}).setdefault(source_package,{}).setdefault(binary_package, "")

# Generate all the index file.
with open(cwd / "roles.rst", "w") as f:
    f.write("Roles (index)\n========================\n\n")
    f.write(".. toctree::\n   :maxdepth: 3\n\n")
    for distribution in roles_index.keys():
        f.write(f"   roles/{distribution}/index\n")
        
for distribution, source_packages in roles_index.items():

    # Generate the distribution.rst file
    with open(cwd / "roles" / distribution / "index.rst", "w") as f:
        f.write(f"{distribution} (Distribution)\n========================\n\n")
        f.write(".. toctree::\n   :maxdepth: 2\n\n")
        for source_package in source_packages.keys():
            f.write(f"   packages/{source_package}/index\n")

    for source_package, binary_packages in source_packages.items():
        # Generate the source_package.rst file
        with open(cwd / "roles" / distribution / "packages" / source_package / "index.rst", "w") as f:
            f.write(f"{source_package} (source package)\n{'=' * len(source_package)}==================\n\n")
            f.write(".. toctree::\n   :maxdepth: 2\n\n")
            for binary_package in binary_packages.keys():
                f.write(f"   {binary_package[:-3]}\n")
