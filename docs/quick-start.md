# Getting Started

## Installation

To install SDL, run the following commands:

```bash
sudo apt update
sudo apt install sdl sdl-roles
```

For detailed installation steps and considerations, refer to the [Installation Guide](install.md).

## Configuration

Configure SDL by creating inventory files locally or using a centralized Git repository. For example, to install `vim` on all hosts, you can create the following inventory file:

```yaml
all:
  vars:
    apt_install_packages_vim:
      - vim
```

For detailed configuration options, see the [Configuration Guide](configuration.md).
