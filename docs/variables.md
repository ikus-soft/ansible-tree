# Defining variables

Understanding how Ansible assigns and manages variables is crucial for writing inventory file for SDL. This document covers the hierarchy of variable precedence, distinctions between different types of values, and best practices for combining variables dynamically in roles. By following these guidelines, you can ensure consistency and scalability in your inventory files.

## 1. Variable Precedence in Ansible

SDL applies variables from multiple sources with a well-defined precedence. The order of precedence (from lowest to highest) is as follows:

1. **Role Defaults** (`defaults/default.yml`) – Applied only if the role is listed in `sdl__use_defaults`.
2. **All Group Variables** – Variables assigned to the `all` group have the lowest precedence. Since all hosts belong to this group, it is a suitable place to override role defaults globally.
3. **Parent Group Variables** – If a host belongs to a group hierarchy, variables defined in parent groups have lower precedence than those in child groups.
4. **Child Group Variables** – Variables from more specific (child) groups take precedence over those from parent groups.
5. **Host-Specific Variables** – Variables defined at the host level override any group variables.

When multiple sources define the same variable, the one with the highest precedence is used.

## 2. Undefined Variables  

In Ansible roles, when a variable is **undefined**, the role takes no action. It's essential to understand the differences between **undefined**, **null**, **false**, and an **empty string**, as they can behave differently in conditionals and role execution.  

### **Undefined**  
A variable that has not been set anywhere.  

- A variable is **undefined** if it is not defined in the role's defaults, inventory, or elsewhere.  
- You can explicitly make a variable undefined using the `undef()` function in an inventory file:  
  ```yaml
  package_name__my_variable: "{{ undef() }}"
  ```  

### **Null**  
Represents an explicit lack of value, similar to `None` in Python.  

- Can be set using any of the following:  
  ```yaml
  package_name__my_variable:
  package_name__my_variable: null
  package_name__my_variable: ~
  package_name__my_variable: "{{ None }}"
  ```  

### **False**  
A Boolean `false` value, often used in conditionals. Ansible considers several values as `false`:  

- Case-insensitive boolean values:  
  ```yaml
  package_name__my_variable: false  # Explicit Boolean
  package_name__my_variable: "false"  # String (also evaluates as false)
  package_name__my_variable: no
  package_name__my_variable: off
  package_name__my_variable: 0  # Integer zero
  ```  

### **Empty String**  
A string with no characters, different from `null` or `false`.  

- Defined explicitly as:  
  ```yaml
  package_name__my_variable: ""
  ```  

Understanding these distinctions ensures better control over role behavior and prevents unintended actions due to misconfigured variables.

## 3. Combined Variable Pattern in Roles

To provide flexibility and modularity, many of our roles use a **combined variable pattern**. This allows merging multiple related variables into a single list or a single key-value pair.

### Example: Package Installation Variables
We define:
- `apt_install_packages_default`: A default list of packages provided by the role.
- `apt_install_packages_*`: Any variable matching the pattern will be merged.
- `apt_install_packages`: A final variable get also merge.

This allows splitting package definitions logically across:
- Inventory files
- Group variables
- Host variables

### Benefits
- **Modularity**: Different groups/hosts can extend the package list without overriding defaults.  
- **Flexibility**: Packages can be defined in different locations and still be combined.  
- **Scalability**: Avoids hardcoding variables in roles, making them adaptable to various environments.

This approach ensures inventory remain reusable and adaptable while allowing customization through inventory and group/host-specific variables.
