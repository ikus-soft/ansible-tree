# Configuration

SDL roles provide a structured and efficient way to configure your system. Each role includes specific default variables that can be adjusted to customize SDL's behavior. For detailed information about available variables and their effects, refer to the documentation provided with each role.

## Defining Inventory

The recommended way to configure SDL is by overriding its default variables with your preferred values. This can be achieved through various methods, depending on your requirements. All methods involve creating and deploying an inventory file to the target systems.

### 1. Locally Deployed Inventory (`/etc/sdl/inventories/` and `/etc/sdl/roles/`)

This method allows you to manage SDL inventory and roles locally. To do so, create any valid ansible inventory file in the `/etc/sdl/inventories/` directory, for example: `/etc/sdl/inventories/my-inventory.yml`. Variables defined in this inventory will be applied whenever `sdl-apply` is executed, either manually using `sdl-apply --all` or automatically when installing or removing packages via `apt install` or `apt remove`.

Additionally, you can extend existing roles or create new ones to configure additional packages by defining local roles in `/etc/sdl/roles/`.

e.g:
```
/etc/sdl/roles/bookworm/packages/nvidia-open/nvidia-open
├── defaults
│   └── default.yml  # Defaults role variables
├── handlers
│   └── main.yml
├── tasks
|   ├── postinst.yml  # Task executed post-install or upgrade
|   ├── postrm.yml  # Tasks executed post-remove
|   └── postpurge.yml # Tasks executed post-purge
```

**Advantages:**
- Simple and flexible for small-scale deployments.

**Disadvantages:**
- Requires manual redistribution of inventory files when changes are made.
- Managing updates can become challenging for large-scale deployments, where maintaining consistency across multiple systems is critical.

### 2. Centralized Using Git (`SDL_GITURL`)

This approach enables centralized management of inventory files and roles through a Git repository. When changes are pushed to the repository, the next execution of `sdl-apply --all` (or when triggered by `apt install` or `apt remove`) will automatically fetch and apply the updates.

To enable this feature, edit `/etc/sdl/sdl-apply.conf` and specify the Git repository URL:

```
# Define a URL to automatically update configuration from.
SDL_GITURL="https://gitlab.com/ikus-soft/ikus-sdl-config.git"
```

**Repository Structure:**

Your Git repository should follow this structure:

```
git root
├── inventories
│   ├── <your-inventory-file.yml>
│   ├── <another-inventory-file.ini>
│   ├── <inventory-script.sh>  # Executable script file
├── roles
│   ├── <debian-distribution>
│   │   ├── packages
│   │   │   ├── <source-package>
│   │   │   │   └── <binary-package>
```

- The `roles` directory is optional and only required if additional roles are needed.
- Inventory files are stored in the `inventories` directory and can be in various formats, including `.yaml`, `.ini`, or executable scripts.

**Advantages:**
- Centralized and scalable, ideal for managing large deployments.
- Simplifies updates and ensures consistency across multiple systems.

**Disadvantages:**
- Requires setting up and maintaining a Git repository.

```{hint}
### Using Both Methods Simultaneously

**Locally Deployed Inventory** and **Centralized Using Git** are **not mutually exclusive**. You can use both methods simultaneously, allowing you to manage certain configurations locally while pulling additional configurations from a Git repository. SDL prioritizes the local inventory if both are defined.
```

```{hint}
## Configuring `SDL_GITURL` Using Debconf

In environments where automation is required, particularly for preseed installations, you can configure `SDL_GITURL` using **debconf**. This is especially useful for automated or large-scale deployments.

To configure it, add the following line to your `preseed.cfg` file: `sdl sdl/giturl string https://gitlab.com/my-user/my-sdl-config.git/`
```

## Customizing Configuration

To begin using SDL, you must define at least two variables in your inventory file. Without these, SDL will not apply any changes to your system.

The following variables can be defined in the Ansible inventory to control SDL's behavior:

### 1. `sdl__manage_packages`

- **Definition**: Specifies the `<source-package>/<binary-package>` names to be managed by SDL.  
- **Matching Variables**: Any variable matching `sdl__manage_packages` or `sdl__manage_packages_*` is combined into a single list of packages to be managed by SDL.
- **Values**: This variable can be defined as follows:
  1. A single package name (e.g., `openssh-client`).
  2. A list of package names (e.g., `["openssh/openssh-client", "apache/apache2", "vim/vim"]`).
  3. A wildcard `*` to apply SDL regardless of the `<source-package>` or `<binary-package>` name (e.g., `openssh/*`).
  4. A package name prefixed with `!` to exclude it from being managed (e.g., `!vim/vim`).

- **Use Case**:  
This variable allows administrators to define complex inventory configurations, enabling different groups of computers to have distinct sets of managed packages.

**Example**:

```yaml
all:
  vars:
    sdl__manage_packages:
      - openssh/openssh-client
      - "apache2/*"  # Manage all binary package where source-package is apache2.
      - "*/vim"  # Manage vim binary package regardless of the source-package.
```

### 2. `sdl__use_defaults`
- **Definition**: Determines whether SDL should load its optimized default values.  
- **Values**: Boolean value `true` or `false`.
- **Behavior**:
  - When defaults are **not loaded**, all relevant variables remain undefined.
  - Undefined variables typically result in no modifications being made to the system by the role.
  - When defaults are **loaded**, the variable take defaults value as documented.

- It's possible to cherrypick which package for which to load defaults using variable `<binary_package_name>__defaults: `


**Example**:

```yaml
all:
  vars:
    sdl__use_defaults: true  # Use default for all package
    openssh_server__defaults: modern-ssh  # Use alternative defaults for openssh-server
    sudo__defaults:  # Don't use defaults for sudo package
```

## Inventory Examples

### Example 1: Manage all packages with optimized defaults

```yaml
all:
  vars:
    sdl__manage_packages: "*/*"
    sdl__use_defaults: true

    apt_install_packages:
      - openssh/openssh-server
      - "*/task-gnome-desktop"

  hosts:
    this-is-my-example.com
```

In this example, SDL will install `openssh-server`, `gnome-desktop`, and all dependencies, applying optimized default values for all installed packages.

### Example 2: Install and manage OpenSSH

```yaml
all:
  vars:
    sdl__manage_packages:
      - "apt/*"
      - openssh/openssh-client
      - openssh/openssh-server
    sdl__use_defaults: true
    
    apt_install_packages:
      - openssh-client
      - openssh-server

  hosts:
    this-is-my-example.com
```

In this example, SDL will install `openssh-client` and `openssh-server` using optimized default values.
