.. Software defined Linux documentation master file, created by
   sphinx-quickstart on Tue Jan  7 15:17:09 2025.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Software Defined Linux (SDL)
====================================

Software Defined Linux (SDL) is a cutting-edge configuration management tool designed to simplify the deployment and maintenance of Linux systems. By leveraging declarative configuration principles, SDL enables system administrators to manage system configurations with precision, consistency, and scalability.

Whether you’re managing a single server or a fleet of workstations, SDL streamlines the process of applying system-wide configurations and ensures they are enforced automatically whenever the system is updated.


.. toctree::
   :caption: User Manual
   :maxdepth: 1
   :hidden:

   quick-start
   install
   configuration
   variables
   roles
