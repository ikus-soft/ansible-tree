# openssh-server

This role configures and secures the OpenSSH server (openssh-server) on Debian-based systems. It provides options for customizing authentication mechanisms, managing SSH keys, and ensuring secure defaults for cryptographic parameters by managing configuration files located at `/etc/ssh/sshd_config` and `/etc/ssh/sshd_config.d/local.conf`.

## Features

### 1. **Modern SSH**
- An additional default for this role is provided to enhance openssh server security, but may limit compatibility with older clients. 
- This settings defines an alternative value for removing obsolete or insecure encryption, disabling RSA and DSA.
- A complete list of variables is provided at the end of this documentation.

e.g.:

```yaml
all:
  vars:
    sdl__manage_packages: openssh/openssh-server
    openssh_server__defaults: modern-ssh
```

### 2. **PAM Configuration**
- Enables `pam_access.so` for access control.
- Disables `pam_motd.so` to reduce login overhead and avoid unnecessary MOTD messages during remote connections.

### 3. **Cryptographic Configuration**
- Validates and updates the Diffie-Hellman moduli to remove weak DH parameters.

**Variables:**
- `openssh_server__moduli_minimum`: Integer. Minimum acceptable bit length for Diffie-Hellman moduli. Default: `2048`.

### 4. **SSH Host Key Management**
- Deletes unwanted host private and public keys based on configuration.
- Collects existing host certificate keys.

**Variables:**
- `openssh_server__host_keys`: List of strings. Specifies the desired host key types (e.g., `['ecdsa', 'rsa', 'ed25519']`).
- `openssh_server__host_keys_delete_unwanted`: Boolean. Controls whether unwanted host keys are deleted.

### 5. **Custom Configuration File**
- Includes custom OpenSSH server configuration files (`sshd_config.d`).

**Templates:**
- `/etc/ssh/sshd_config.d/local.conf`: Custom configuration file to be validated with `sshd -t`.

**Variables:**
- `openssh_server__password_authentication`: Boolean. Enables or disables password authentication. Default: `False`
- `openssh_server__permit_root_login`: String. Specifies whether root login is allowed (e.g., `yes`, `no`, `without-password`).
- `openssh_server__accept_env`: List. Define a list of AcceptEnv. Default: `TERM`.
- `openssh_server__authorized_keys_file`: List. Define list of AuthorizedKeysFile. Default: `/etc/ssh/authorized_keys/%u` and `.ssh/authorized_keys`.
- `openssh_server__allow_agent_forwarding`: Boolean. Allow or block agent forwarding. Default value: `False`.
- `openssh_server__debian_banner`: Boolean. Display or hide Debian banner. Default value: `False`
- `openssh_server__login_grace_time`: Time Unit. Limits authentication time for clients. Default: `1m`
- `openssh_server__max_auth_tries`: Integer. Limits failed authentication attempts allowed. Default: `3`
- `openssh_server__ca_signature_algorithms`: List. Specifies the algorithms allowed for signing certificates by certificate authorities.
- `openssh_server__ciphers`: List. Specifies the ciphers allowed.
- `openssh_server__hostbased_accepted_algorithms`: List. Specifies the signature algorithms accepted for host-based authentication.
- `openssh_server__host_key_algorithms`: List. Specifies the host key signature algorithms that the client wants to use in order of preference.
- `openssh_server__kex_algorithms`: List. Specifies the available KEX (Key Exchange) algorithms.
- `openssh_server__macs`: List. Specifies the MAC (message authentication code) algorithms in order of preference.
- `openssh_server__pubkey_accepted_algorithms`: List. Specifies the signature algorithms that will be used for public key authentication.

## Handlers

When changes are made to OpenSSH server configuration, the server is automatically restarted at the end.

## Example Inventory


### 1. Use modern-ssh

```yaml
all:
    hosts:
        my-host.example.com:
    vars:
        sdl__managed_packages: openssh/openssh-server
        openssh_server__defaults: modern-ssh
        openssh_server__password_authentication: true  # Enable password-based authentication
        openssh_server__permit_root_login: "without-password"  # Restrict root login
```

## 2. Use default

To support RSA and DSA key exchanges while using secure ciphers and algorithms, configure as follows:

```yaml
all:
    hosts:
        my-host.example.com:
    vars:
        sdl__managed_packages: openssh/openssh-server
        sdl__use_defaults: true
        openssh_server__host_keys: ["ed25519", "rsa", "ecdsa"]  # Allow specific key types
        openssh_server__pubkey_accepted_algorithms:  # Use Debian default value
```


