# openssh-client

This role configures and secures the OpenSSH client (`openssh-client`) on Debian-based systems. It provides options for enhancing security by disabling obsolete cryptographic features, customizing identity and authentication mechanisms, and managing client-specific configuration files located at `/etc/ssh/ssh_config.d/local.conf`.

## Features

### 1. **Modern SSH**
- An additional default for this role is provided to enhance openssh client security, but may limit compatibility with older servers. 
- This settings defines an alternative value for removing obsolete or insecure encryption, disabling RSA and DSA.
- A complete list of variables is provided at the end of this documentation.

e.g.:

```yaml
all:
    vars:
        sdl__manage_packages: openssh/openssh-client
        openssh_client__defaults: modern-ssh
```

### 2. **Custom Configuration File**
- Includes client-specific OpenSSH configuration files (`ssh_config.d`).

**Templates:**
- `/etc/ssh/ssh_config.d/local.conf`: Custom configuration file to be included in the client configuration.

**Variables:**
- `openssh_client__identity_file`: List of strings. Specifies the files used for user identity.
- `openssh_client__ca_signature_algorithms`: List of strings. Specifies allowed CA signature algorithms.
- `openssh_client__ciphers`: List of strings. Specifies allowed encryption ciphers.
- `openssh_client__hostbased_accepted_algorithms`: List of strings. Specifies accepted host-based signature algorithms.
- `openssh_client__host_key_algorithms`: List of strings. Specifies preferred host key signature algorithms.
- `openssh_client__kex_algorithms`: List of strings. Specifies allowed KEX algorithms.
- `openssh_client__macs`: List of strings. Specifies preferred MAC algorithms.
- `openssh_client__pubkey_accepted_algorithms`: List of strings. Specifies accepted public key authentication algorithms.

### 3. **CA Certificate Management**
- Adds a Certificate Authority (CA) certificate to the `/etc/ssh/ssh_known_hosts` file.

**Variables:**
- `openssh_client__known_hosts_cert_authority`: String. Specifies the CA certificate entry.

## Example Inventory

Revert to Debian's default settings by disabling modern SSH features:

```yaml
all:
    hosts:
        client-host.example.com:
    vars:
        openssh_client__modern_ssh: false
```

Customize allowed identity files and public key algorithms:

```yaml
all:
    hosts:
        client-host.example.com:
    vars:
        openssh_client__identity_file: ["~/.ssh/id_rsa"]
        openssh_client__pubkey_accepted_algorithms: ["rsa-sha2-256", "rsa-sha2-512"]
```
