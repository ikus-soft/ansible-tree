from ansiblelint.rules import AnsibleLintRule


class NoDebugRule(AnsibleLintRule):
    id = "sdl-no-debug"
    shortdesc = "Disallow use of debug module"
    description = "Tasks should not use the debug module in production playbooks."
    severity = "HIGH"
    tags = ["debug"]
    version_changed = "1.0.0"

    def matchtask(self, task, file):
        return task.get("action", {}).get("__ansible_module__", "") == "debug"
