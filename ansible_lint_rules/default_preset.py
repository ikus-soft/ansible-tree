import os

from ansiblelint.rules import AnsibleLintRule


class DefaultsRule(AnsibleLintRule):
    id = "sdl-defaults"
    shortdesc = "Ensure role has a defaults/default.yml file"
    description = "Checks that each role contains a defaults/default.yml file."
    severity = "HIGH"
    tags = ["role_structure"]
    version_changed = "1.0.0"

    def matchplay(self, file, data):
        # Only process tasks files.
        if file.kind != "tasks":
            return []

        # Check if the role directory contains a defaults/main.yml file
        defaults_main_yml_path = os.path.normpath(
            file.name + "/../../defaults/default.yml"
        )

        # If exists, no error
        if os.path.exists(defaults_main_yml_path):
            return []

        return [
            self.create_matcherror(
                message=self.shortdesc,
                filename=file,
                tag=f"{self.id}",
            )
        ]
