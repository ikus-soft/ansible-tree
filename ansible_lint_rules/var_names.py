from ansiblelint.rules import AnsibleLintRule


class VariableNamingRule(AnsibleLintRule):
    id = "VARIABLE_NAMING"
    shortdesc = "Variables in defaults must start with role name"
    description = "Ensure all variables in defaults/main.yml start with <role_name>__"
    tags = ["naming", "defaults"]

    def matchyaml(self, file):
        # Skip files outside the 'defaults' directory
        if file.kind != "vars":
            return []

        role_name = file.name.split("/")[
            -3
        ]  # assuming file path is like <role>/defaults/main.yml
        role_name = role_name.replace("-", "_")  # Replace hyphens with underscores

        if not isinstance(file.data, dict):
            return []

        # Determien the expected variable name
        kind = file.name.split("/")[-2]
        if kind == "vars":
            expected_pattern = f"_{role_name}__"
        else:
            expected_pattern = f"{role_name}__"

        matches = []
        for var in file.data.keys():
            # Ignore internal variables
            if var in ["__file__", "__line__", "__skipped_rules__"]:
                continue

            # Check our patterns.
            if not var.startswith(expected_pattern):
                matcherror = self.create_matcherror(
                    message=f"Variable '{var}' does not start with role name '{expected_pattern}'",
                    filename=file,
                )
                matches.append(matcherror)
        return matches
