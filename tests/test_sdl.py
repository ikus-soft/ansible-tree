import glob
import os
import shutil
import subprocess
import sys
import tempfile
import unittest
from pathlib import Path

from parameterized import parameterized_class

# List availables roles
tests = []
roles_dir = os.path.normpath(os.path.join(__file__, "../../roles"))
for fn in glob.glob("**/tests/*.y*ml", root_dir=roles_dir, recursive=True):
    distribution = fn.rstrip("/").split("/", 1)[0]
    tests.append((distribution, fn))
tests = sorted(tests)

# Get location of sdl-apply
sdl_apply_src = os.path.normpath(os.path.join(__file__, "../../sdl-apply/"))

def name_func(cls, num, params_dict):
    distribution, _, _, binary_package, _, test_file = params_dict['test_file'].split("/", 6)
    suffix = (
        f"{distribution}_{binary_package}_{test_file.replace('.yml','')}"
    )
    return "%s_%s" %(
        cls.__name__,
        suffix
    )


class AbstractChrootTest(unittest.TestCase):
    distribution = None

    def _run(self, *args, **kwargs):
        try:
            return subprocess.run(*args, **kwargs, capture_output=True, check=True)
        except subprocess.CalledProcessError as e:
            # Format a nice error message to help debug.
            cmd = " ".join(e.cmd)
            msg = 'Command "%s" returned non-zero exit status: %s\n' % (
                cmd,
                e.returncode,
            )
            if e.stdout:
                msg += "stdout:\n"
                msg += e.stdout.decode()
            if e.stderr:
                msg += "stderr:\n"
                msg += e.stderr.decode()
            self.fail(msg)

    
    def setUp(self):
        self.target = tempfile.mkdtemp(prefix="ansible-chroot-%s" % (self.distribution))
        # Check if cached
        cache_dir = Path(tempfile.tempdir) / (
            "ansible-chroot-cache-%s" % (self.distribution)
        )
        if not cache_dir.exists():
            mmdebstrap_cmd = [
                "mmdebstrap",
                "--format=directory",
                "--mode=root",
                "--variant=apt",
                "--skip=check/empty",
                "--include=ansible,python3-apt,procps",
                '--aptopt=Apt::Install-Recommends "false"',
                '--aptopt=APT::Sandbox::User "root"',
                self.distribution,
                cache_dir,
            ]
            self._run(mmdebstrap_cmd)
            (cache_dir / "run/sshd").mkdir(exist_ok=True)
        # Then create a copy of the cache
        self._run(["rsync", "-a", "%s/" % cache_dir, self.target])
        # Given sdl-apply is installed
        self._run(["rsync", "-a", sdl_apply_src + "/", self.target])
        # Create /dev/shm for multiprocessing used by ansible
        self._run(["mount", "--bind", "/dev", "%s/dev" % self.target])
        self._run(["mount", "--bind", "/dev/pts", "%s/dev/pts" % self.target])
        self._run(["mount", "--bind", "/proc", "%s/proc" % self.target])
        self._run(["mount", "--bind", "/sys", "%s/sys" % self.target])

    def tearDown(self):
        self._run(["umount", "%s/dev/pts" % self.target])
        self._run(["umount", "%s/dev" % self.target])
        self._run(["umount", "%s/proc" % self.target])
        self._run(["umount", "%s/sys" % self.target])
        # Delete the environment.
        shutil.rmtree(self.target)

@parameterized_class(('distribution'), [('bookworm',), ('trixie',)])
class TestSdlApply(AbstractChrootTest):

    def test_sdl_install(self):
        # Given roles are not installed.
        # When executing `sdl-apply --install openssh/openssh-server`
        self._run(
            [
                "/usr/sbin/chroot",
                self.target,
                "sdl-apply",
                "--install",
                "openssh/openssh-server"
            ],
            env={
                "HOME": "/root/",
                "USER": "root",
                "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "TERM": os.environ.get("TERM", "xterm-mono"),
                "LANG": "C.UTF-8",
            },
        )
        # Then no exception are raised.

    def test_sdl_apply_all(self):
        # When executing `sdl-apply --all`
        self._run(
            [
                "/usr/sbin/chroot",
                self.target,
                "sdl-apply",
                "--all"
            ],
            env={
                "HOME": "/root/",
                "USER": "root",
                "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "TERM": os.environ.get("TERM", "xterm-mono"),
                "LANG": "C.UTF-8",
            },
        )
        # Then no exception are raised.
        # Then a `lastrun` file get create
        lastrun_file = (Path(self.target) / "var/lib/sdl/lastrun")
        self.assertTrue(lastrun_file.exists())


@parameterized_class(('distribution', 'test_file'),
                     tests,  class_name_func=name_func)
class TestRoles(AbstractChrootTest):
    test_file = None

    def setUp(self):
        # Given a debootstrap debian with sdl-apply installed
        super().setUp()
        # Given our ansible playbook are installed
        self._run(
            [
                "rsync",
                "-a",
                roles_dir + "/",
                os.path.join(self.target, "usr/share/sdl/roles/"),
            ]
        )
        # Enforce permissions to avoid ansible warning
        os.chmod(os.path.join(self.target, "usr/share/sdl/roles"), 0o664)

    
    def test_roles(self):
        # When installing the package
        self._run(
            [
                "/usr/sbin/chroot",
                self.target,
                "sh",
                "-c",
                "cd /usr/share/sdl/roles && ansible-playbook %s --connection=local --limit localhost --inventory localhost,"
                % self.test_file,
            ],
            env={
                "ANSIBLE_LIBRARY": "/usr/share/sdl/library",
                "ANSIBLE_ROLES_PATH": "/usr/share/sdl/roles",
                "HOME": "/root/",
                "USER": "root",
                "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "TERM": os.environ.get("TERM", "xterm-mono"),
                "LANG": "C.UTF-8",
            },
        )



if __name__ == "__main__":
    if os.geteuid() != 0:
        print("Not running as root.")
        sys.exit(1)
    unittest.main()
