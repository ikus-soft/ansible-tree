#
# Custom module to compare two file list.
#

import fnmatch
import os

from ansible.module_utils.basic import AnsibleModule

DEFAULT_EXCLUDES = [
    "/dev",
    "/sys",
    "/proc",
    "/var/log",
    "/tmp/ansible_*",
    "/root/.ansible",
    "/var/cache",
    "/var/lib/apt",
    "/var/lib/dpkg",
    "/etc/shadow-",
    "/etc/passwd-",
    "/etc/group-",
    "/etc/gshadow-",
    "/var/lib/sdl/lastrun",
]


def is_ignored(path, excludes):
    for pattern in excludes:
        if fnmatch.fnmatch(path, pattern):
            return True
    return False


def get_file_info(directory, excludes):
    """
    Collect file information with file size and file mode.
    """
    file_info = {}
    for root, dirs, files in os.walk(directory):
        dirs[:] = [d for d in dirs if not is_ignored(os.path.join(root, d), excludes)]
        for fn in files:
            path = os.path.join(root, fn)
            if not is_ignored(path, excludes):
                file_stat = os.lstat(path)
                file_info[path] = {
                    "size": file_stat.st_size,
                    "mode": oct(file_stat.st_mode & 0o777),
                }
    return file_info


def run_module():
    module_args = dict(
        path=dict(type="str", required=True),
        before=dict(type="dict", required=False, default=None),
        excludes=dict(type="list", elements="str", required=False, default=[]),
        default_excludes=dict(
            type="list", elements="str", required=False, default=DEFAULT_EXCLUDES
        ),
    )

    result = dict(changed=False, files={}, added=[], removed=[], changed_files={})

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    path = module.params["path"]
    before_info = module.params["before"]
    excludes = [
        os.path.abspath(path) for path in module.params["excludes"]
    ] + module.params["default_excludes"]

    if before_info is None:
        result["files"] = get_file_info(path, excludes)
    else:
        after_info = get_file_info(path, excludes)
        added = set(after_info.keys()) - set(before_info.keys())
        removed = set(before_info.keys()) - set(after_info.keys())
        changed = {
            path: {
                "size_before": before_info[path]["size"],
                "size_after": after_info[path]["size"],
                "mode_before": before_info[path]["mode"],
                "mode_after": after_info[path]["mode"],
            }
            for path in set(before_info.keys()) & set(after_info.keys())
            if before_info[path]["size"] != after_info[path]["size"]
            or before_info[path]["mode"] != after_info[path]["mode"]
        }
        result["files"] = after_info
        result["added"] = list(added)
        result["removed"] = list(removed)
        result["changed_files"] = changed
        result["changed"] = bool(added or removed or changed)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
