# Software Defined Linux (SDL)

**Software Defined Linux (SDL)** is an open-source project designed to manage the configuration of a fleet of Debian-based systems by defining configurations in YAML files directly on each machine. SDL ensures that specific configurations are applied when running common package management commands (`apt install`, `apt upgrade`, `apt remove`, `apt purge`, etc.). By leveraging Ansible under the hood, SDL automates and enforces configurations across systems, making management more efficient and consistent.

## Features

- **YAML Configuration Management**: Define system configurations in YAML files directly on the managed systems.
- **Automated Configuration Updates**: Automatically apply configurations during common package management commands.
- **Ansible-Driven**: Uses Ansible to implement and enforce configurations, taking advantage of its robust automation capabilities.
- **Fleet-Wide Consistency**: SDL ensures that configurations are applied consistently across all Debian-based systems in your environment.
- **Open Source**: Released under the GNU General Public License (GPL), allowing for community contributions and transparency.

## License

This project is licensed under the GNU General Public License (GPL).

## Acknowledgments

Special thanks to our main sponsor, **Bern University of Applied Sciences (BFH)**, for their support in making this project possible.
